import numpy as np
import os
import subprocess

from osgeo import gdal

def calc_average(filenames, outfile):
    """
    Caclulates average of a stack of images using GDAL and Numpy, ignoring nodata values.
    :param filenames: list of filepaths of images to average.
    :param outfile: filepath of output file to create.
    """
    # get number of bands and set up output file
    ds = gdal.Open(filenames[0])
    band_count = ds.RasterCount
    driver = gdal.GetDriverByName('GTiff')
    result = driver.CreateCopy(outfile, ds, 0, ["TILED=YES", "COMPRESS=DEFLATE", "PREDICTOR=2", "BIGTIFF=YES"])

    # average each band separately
    for band in range(1, band_count + 1):
        rasters = []
        # get all raster data from inputs
        for f in filenames:
            ds = gdal.Open(f)
            raster = ds.GetRasterBand(band).ReadAsArray()
            nodata = ds.GetRasterBand(band).GetNoDataValue()
                        
            raster[raster == nodata] = np.nan  # convert nodata value to nan
            rasters.append(raster)
         
        # stack the inputs and perform mean (ignoring nans)
        stacked = np.dstack(rasters)
        mean = np.nanmean(stacked, axis=-1)
        mean[mean == np.nan] = nodata  # transform back to nodata

        # write mean into output file
        result.GetRasterBand(band).WriteArray(mean)
        result.GetRasterBand(band).SetNoDataValue(nodata)
        
    result = None  # clear output from memory


def calc_difference(input1, input2, outfile):
    """
    Caclulates the difference between two images (subtracts 2nd image from 1st)
    :param input1: filepath of first image to difference.
    :param input2: filepath of second image to difference.
    :param outfile: filepath of output TIFF. Result = input1 - input2
    """    
    # get number of bands and set up output file
    ds = gdal.Open(input1)
    band_count = ds.RasterCount
    driver = gdal.GetDriverByName('GTiff')
    
    # create output file as direct copy of input1
    result = driver.CreateCopy(outfile, ds, 0, ["TILED=YES", "COMPRESS=DEFLATE", "PREDICTOR=2", "BIGTIFF=YES"])

    # average each band separately
    for band in range(1, band_count + 1):
        ds1 = gdal.Open(input1)
        raster1 = ds1.GetRasterBand(band).ReadAsArray()
        nodata1 = ds1.GetRasterBand(band).GetNoDataValue()
        
        ds2 = gdal.Open(input2)
        raster2 = ds2.GetRasterBand(band).ReadAsArray()
        nodata2 = ds2.GetRasterBand(band).GetNoDataValue()
        
        raster1[raster1 == nodata1] = np.nan  # convert nodata value to nan
        raster2[raster2 == nodata2] = np.nan  # convert nodata value to nan
       
        diff = np.subtract(raster1, raster2)
        diff[diff == np.nan] = nodata1  # transform back to nodata

        # write mean into output file
        result.GetRasterBand(band).WriteArray(diff)
        result.GetRasterBand(band).SetNoDataValue(nodata1)
        
    result = None  # clear output from memory
    
    
def calc_threshold(in_image, thresh_image, band=1, thresh_val=-0.015):
    """
    Caclulates a binary threshold less than a specified value
    :param in_image: filepath of image to threshold
    :param thresh_image: filepath of output binary TIFF. 
    :param band: band number of input file to work on
    :param thresh_val: threshold value. Values LESS than this will be set to 1, otherwise 0
    """    
    ds = gdal.Open(in_image)  # open reference image
    driver = gdal.GetDriverByName('GTiff')
    
    # create empty output file
    result = driver.Create(thresh_image, ds.RasterXSize, ds.RasterYSize, 1, gdal.GDT_Byte, options=["TILED=YES", "COMPRESS=DEFLATE", "PREDICTOR=2", "BIGTIFF=YES"])
    result.SetGeoTransform(ds.GetGeoTransform())
    result.SetProjection(ds.GetProjectionRef())

    # load raster data
    raster = ds.GetRasterBand(band).ReadAsArray()
    nodata = ds.GetRasterBand(band).GetNoDataValue()
    
    # convert nodata value to nan
    raster[raster == nodata] = np.nan  
   
    # calculate threshold 
    thresh = np.less(raster, thresh_val)
    thresh[thresh == np.nan] = 255 

    # write mean into output file
    result.GetRasterBand(band).WriteArray(thresh)
    result.GetRasterBand(band).SetNoDataValue(255)
        
    result = None  # clear output from memory
    

def detect_first_burn(image_value_dict, outfile):
    """
    Replaces 1's in binary images with associated values, then finds minimum over the stack
    :param image_value_dict: dict with binary image filepaths as keys, and values as numbers to substitute 
    :param outfile: filepath of output image
    """    
    ds = gdal.Open(list(image_value_dict)[0])  # open reference image
    driver = gdal.GetDriverByName('GTiff')
    
    # create empty output file
    result = driver.Create(outfile, ds.RasterXSize, ds.RasterYSize, 1, gdal.GDT_UInt32, options=["TILED=YES", "COMPRESS=DEFLATE", "PREDICTOR=2", "BIGTIFF=YES"])
    result.SetGeoTransform(ds.GetGeoTransform())
    result.SetProjection(ds.GetProjectionRef())

    rasters = []
    for image in image_value_dict:
        ds = gdal.Open(image)
        raster = ds.GetRasterBand(1).ReadAsArray().astype(np.uint32)  # read threshold images and convert to necessary data type
        nodata = ds.GetRasterBand(1).GetNoDataValue()
        
        raster[raster == nodata] = 999999999  # convert nodata value to maximum possible value
        raster[raster == 0] = 999999999  # convert zero data to maximum possible value
        raster[raster == 1] = int(image_value_dict[image])  # replace values of 1 with value from dict for this image
        rasters.append(raster)

    # stack the inputs and perform min (ignoring nans)
    stacked = np.dstack(rasters)
    minimum = np.nanmin(stacked, axis=-1)
    minimum[minimum == 999999999] = 0  # anywhere that remains the maximum possible value is nodata and should be converted back
   
    # write mean into output file
    result.GetRasterBand(1).WriteArray(minimum)
    result.GetRasterBand(1).SetNoDataValue(0)
        
    result = None  # clear output from memory
    

def gdalwarp(in_raster, out_raster, dst_srs=None, src_nodata=None, dst_nodata=None, clip_vector=None, resolution=None, overwrite=False):
    """
    A simple python wrapper for the gdalwarp commandline utility
    :param in_raster: filepath of input image
    :param out_raster: filepath of output image
    :param dst_srs: (optional) projection to reproject to (in GDAL-compatible notation)
    :param src_nodata: (optional) override detected input nodata
    :param dst_nodata: (optional) set output value as nodata
    :param clip_vector: (optional) filepath of vector to clip output to 
    :param resolution: (optional) output pixel resolution in projected units
    :param overwrite: set to True to force overwrite existing file at out_raster
    """    
    gdal_command = ["gdalwarp"]

    if dst_srs is not None:
        gdal_command.extend(["-t_srs", dst_srs])

    if src_nodata is not None:
        gdal_command.extend(["-srcnodata", str(src_nodata)])

    if dst_nodata is not None:
        gdal_command.extend(["-dstnodata", str(dst_nodata)])

    if clip_vector is not None:
        # clip to vector geometry
        gdal_command.extend(["-cutline", clip_vector, "-crop_to_cutline", "-wo", "CUTLINE_ALL_TOUCHED=TRUE"])

    if resolution is not None:
        gdal_command.extend(["-tr", str(resolution), str(resolution), "-tap"])

    if overwrite:
        gdal_command.append("-overwrite")
        
    gdal_command.extend(["-of", "GTiff", "-multi", "-wo", "NUM_THREADS=ALL_CPUS", "-co", "BIGTIFF=YES", "-co", "TILED=YES", "-co", "COMPRESS=DEFLATE", "-co", "PREDICTOR=2", "-co", "NUM_THREADS=ALL_CPUS"])

    gdal_command.append(in_raster)
    if not out_raster.endswith(".tif"):
        out_raster += ".tif"
    gdal_command.append(out_raster)

    subprocess.check_output(gdal_command)
    

def make_directory(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)


