"""
# # # # # # # # # # # # # #
:author Environment Systems Ltd.
:date 14/03/2022
The Software is provided "as is", with all faults, defects and errors, and without warranty of any kind.


This script is designed to be used in congunction with the burn_dectection_pt1_average.py script. It will create thresholds
and a vector of first detected changed from a set of Sentinel-1 difference products.

Dependencies:
- Python >= 3.6
- GDAL >= 2.2.3

The script must be run with upland_utils.py in the same folder


Script requires only an input directory which MUST be the "02_Difference" output folder from the previous script.

The script allows mean and difference calculations to occur on multiple cores. Note, this will only multiply the number of images 
that are processed simultaneously. The user should ensure that they have sufficient memory and hard drive capabilities to use 
this to produce meaningful benefits.

Then run
python3 /path/to/burn_detection_pt2_thresholds.py -i /path/to/02_Difference

For help with additional arguments, see
python3 /path/to/burn_detection_pt2_thresholds.py --help

=======
Version 1.0
+ Build main workflow

Version 1.1
+ Refine default threshold parameter

# # # # # # # # # # # # # #
"""

import argparse
import multiprocessing
import os
import subprocess
import upland_utils


EXTENSIONS = [".tif"]

def __get_arg_parser():
    parser = argparse.ArgumentParser(description="Run burn detection from S1 backscatter")

    parser.add_argument('-i', '--indir', required=True, help="(Required) Input directory to recursively search for processed S1 imagery. All inputs must be of equal dimensions! Use AOI and RESOLUTION if not.")
    parser.add_argument('-s', '--threshold', default=-0.015, type=float, help="(Optional) Threshold value for detecting burns (Default: %(default)f)")
    
    parser.add_argument('-p', '--threads', default=1, type=int, help="(Optional) Number of concurrent calculations to make (WARNING: increasing could be memory intensive) (Default: %(default)d)")
    
    return parser


def extract_date_from_difference_image(diff_in):
    """
    Pulls out a date from the expected difference image name: 20180924 from S1_20180910-20180923_Subtract_20180924-20181007.tif
    :param diff_in: filename of difference input image
    :return: integer of date 
    """
    return int(os.path.basename(diff_in)[30:38])


def run_difference_and_threshold(diff_in, thresh_out, threshold=-0.015):
    """
    Runs through the steps needed to create a difference file, and threshold the output to map change
    :param diff_in: image to threshold
    :param thresh_out: output TIFF filepath to store difference threshold
    """
    upland_utils.calc_threshold(diff_in, thresh_out, thresh_val=threshold)
    subprocess.check_call(["gdal_sieve.py", "-q", "-st", "10", "-4", thresh_out])


def run(indir, threads=1, threshold=-0.015):
    # get images from the input folder
    images = [os.path.join(root, f) for root, dirs, files in os.walk(indir) for f in files if f.endswith(tuple(EXTENSIONS))]
    sorted_images = sorted(images)  # a basic sort should sort by date if named as expected
    
    thresh_outdir = os.path.join(os.path.dirname(indir), "03_Threshold")
    upland_utils.make_directory(thresh_outdir)
    thresholds_and_dates = {}
    
    pool = multiprocessing.Pool(processes=threads)
    
    print(f"Creating thresholds of regions less than {threshold}...")
    for img in sorted_images:
        thresh_out_name = os.path.join(thresh_outdir, f"{os.path.splitext(os.path.basename(img))[0]}_thresh{threshold}.tif")
        
        pool.apply_async(run_difference_and_threshold, args=(img, thresh_out_name, threshold))
        
        # associate a single date with the threshold
        thresholds_and_dates[thresh_out_name] = extract_date_from_difference_image(img) 

    pool.close()
    pool.join()
    
    # create a map of the earliest detected burn date from stack of thresholds
    print(f"Creating map of first detected burn...")
    change_outdir = os.path.join(os.path.dirname(indir), "04_Detected_Changes")
    upland_utils.make_directory(change_outdir)
    
    change_output = os.path.join(change_outdir, f"S1_{list(thresholds_and_dates.values())[0]}-{list(thresholds_and_dates.values())[-1]}_thresh{threshold}_first_detected_change.tif")
    upland_utils.detect_first_burn(thresholds_and_dates, change_output)
    
    change_output_vector = change_output.replace(".tif", ".gpkg")
    if os.path.isfile(change_output_vector):
        os.remove(change_output_vector)
    
    # convert to vector
    subprocess.check_call(["gdal_polygonize.py", "-q", change_output, "-b", "1", "-f", "GPKG", change_output_vector, os.path.basename(change_output_vector), "Detection"])
    
    
def main():
    args = __get_arg_parser().parse_args()
    
    indir = os.path.abspath(args.indir)
 
    threads = args.threads if args.threads < multiprocessing.cpu_count() else multiprocessing.cpu_count()

    run(indir, threads=threads, threshold=args.threshold)


if __name__ == "__main__":
    main()

