"""
# # # # # # # # # # # # # #
:author Environment Systems Ltd.
:date 24/02/2022
The Software is provided "as is", with all faults, defects and errors, and without warranty of any kind.


Script to generate mean and difference images from Sentinel-1 Linear Backscatter products.

Dependencies:
- Python >= 3.6
- GDAL >= 2.2.3

The script must be run with upland_utils.py in the same folder


Script should be simple to use, requiring only an input directory of linear backscatter .tif files.
A temporary directory can optionally be provided to direct intermediate files to a faster drive.
Outputs will be created within named folders in the output directory.
 
All images MUST be of the same dimensions in oder for calculations to take place. To aid with this, the user can provide an AOI
to clip products to, as well as a target resolution and projection. Using all three of there will guarantee inputs are transformed
to common extents. These arguments are optional, in case imagery has already been prepared in this manner.

The user can adjust the date-pos to target the poistion of the date within the input filenames. The script has been designed for
use with products folling the ESA convention (e.g. "S1A_IW_GRDH_1SDV_20201001T062920_20201001T062945_034599_040733_5AB9") where
the date is accessible from the 18th character. If inputs are different to this format, please set the parameter accordingly

The script allows mean and difference calculations to occur on multiple cores. Note, this will only multiply the number of images 
that are processed simultaneously. The user should ensure that they have sufficient memory and hard drive capabilities to use 
this to produce meaningful benefits.

Then run
python3 /path/to/burn_detection_pt1_average.py -i /path/to/backscatter_tiffs

For help with additional arguments, see
python3 /path/to/burn_detection_pt1_average.py --help

=======
Version 1.0
+ Build main workflow

Version 1.1
+ Split thresholding into a separate script to allow for quicker tuning of parameters from the same set of products. 

# # # # # # # # # # # # # #
"""

import argparse
import datetime
import multiprocessing
import os
import shutil
import subprocess
import tempfile
import upland_utils


EXTENSIONS = [".tif"]

def __get_arg_parser():
    parser = argparse.ArgumentParser(description="Run burn detection from S1 backscatter")

    parser.add_argument('-i', '--indir', required=True, help="(Required) Input directory to recursively search for processed S1 imagery. All inputs must be of equal dimensions! Use AOI and RESOLUTION if not.")
    
    parser.add_argument('-o', '--outdir', help="(Optional) Output directory to export imagery - if not provided, indir is used")
    parser.add_argument('-t', '--tmpdir', help="(Optional) Temporary directory to store intermediate files")
    
    parser.add_argument('-a', '--aoi', help="(Optional) Vector filepath to clip output images to")
    parser.add_argument('-r', '--resolution', default=None, type=float, help="(Optional) Resolution to resample into data to")
    parser.add_argument('-e', '--epsg', help="(Optional) EPSG code (e.g. '27700') for output files")
    
    parser.add_argument('-d', '--date-pos', default=18, type=int, help="(Optional) Start position of YYYYMMDD date in input filenames (e.g. Enter '18' for 'S1A_IW_GRDH_1SDV_20201001T062920_20201001T062945_034599_040733_5AB9') (Default: %(default)d)")
    
    parser.add_argument('-w', '--window-size', default=14, type=int, help="(Optional) Number of days to increment to collect imagery for each mean output (Default: %(default)d)")
    
    parser.add_argument('-p', '--threads', default=1, type=int, help="(Optional) Number of concurrent calculations to make (WARNING: increasing could be memory intensive) (Default: %(default)d)")
    
    return parser



def get_s1_start_date(filename, date_start_pos=17, date_length=8, date_format="%Y%m%d"):
    """
    Extracts the date into a datetime object from the name of a file, based on the expected position and string format
    :param filename: filename (no directory) to extract date from
    :param date_start_pos: zero-indexed position of first character in filename representing date
    :param date_length: length of date string
    :param date_format: datetime format of string
    :return: datetime object representing date extracted from filename
    """
    return datetime.datetime.strptime(filename[date_start_pos:date_start_pos+date_length], date_format)


def run(indir, tmpdir, outdir, aoi=None, resolution=None, epsg=None, threads=1, window_size=14, date_start_pos=17):

    temp_dir = tempfile.mkdtemp(dir=tmpdir)
    images = [os.path.join(root, f) for root, dirs, files in os.walk(indir) for f in files if f.endswith(tuple(EXTENSIONS))]
    resampled_images = []
    
    # ensure all images have the same dimensions by clipping to a common AOI and rsolution
    print(f"Converting input images to common dimensions...")
    for img in images:
        tmp_img = f"{img.replace(indir, temp_dir)}_tmp.tif"
        clip_img = f"{img.replace(indir, temp_dir)}"
        upland_utils.make_directory(os.path.dirname(tmp_img))
        
        # reproject first, then clip. Trying to do both at once results in inconsistent pixel spacing
        dst_srs = f"EPSG:{epsg}" if epsg is not None else None
        upland_utils.gdalwarp(img, tmp_img, dst_srs=dst_srs, resolution=resolution)
        upland_utils.gdalwarp(tmp_img, clip_img, clip_vector=aoi)
        os.remove(tmp_img)
        resampled_images.append(clip_img)

    # sort found images by date
    sorted_images = sorted(resampled_images, key=lambda f: get_s1_start_date(os.path.basename(f), date_start_pos))

    mean_images = []
    mean_outdir = os.path.join(outdir, "01_Mean")
    upland_utils.make_directory(mean_outdir)

    # get start and end of timeseries
    overall_start_date = get_s1_start_date(os.path.basename(sorted_images[0]), date_start_pos)
    overall_end_date = get_s1_start_date(os.path.basename(sorted_images[-1]), date_start_pos)
    
    mean_groups = []
    
    print(f"Creating means every {window_size} days between {overall_start_date.strftime('%d-%m-%Y')} and {overall_end_date.strftime('%d-%m-%Y')}...")
    
    current_date = overall_start_date
    
    # move through all date windows and create means from all imagery that fits into the period
    while current_date <= (overall_end_date - datetime.timedelta(days=window_size)):
        
        images_to_process = [f for f in sorted_images if current_date <= get_s1_start_date(os.path.basename(f), date_start_pos) < current_date + datetime.timedelta(days=window_size)]
                
        if len(images_to_process) > 0:
            mean_groups.append(images_to_process)
            
        current_date += datetime.timedelta(days=window_size)

    pool = multiprocessing.Pool(processes=threads)
    
    # create threads for running mean images
    for images_to_process in mean_groups:
        start_date = get_s1_start_date(os.path.basename(images_to_process[0]), date_start_pos)
        end_date = get_s1_start_date(os.path.basename(images_to_process[-1]), date_start_pos)
        out_name = os.path.join(mean_outdir, f"S1_{start_date.strftime('%Y%m%d')}_{end_date.strftime('%Y%m%d')}_mean.tif")
        
        pool.apply_async(upland_utils.calc_average, args=(images_to_process, out_name))
        mean_images.append((out_name, start_date, end_date))
        
    pool.close()
    pool.join()
    
    shutil.rmtree(temp_dir)  # resampled images are no longer needed and can be removed
    
    pool = multiprocessing.Pool(processes=threads)
        
    diff_outdir = os.path.join(outdir, "02_Difference")
    upland_utils.make_directory(diff_outdir)
        
    print(f"Running difference calculations between adjacent pairs of means and thresholding...")
    
    thresholds_and_dates = {}
    for mean_tuple1 in mean_images:
        mean_img1 = mean_tuple1[0]
        later_images = [f for f in mean_images if f[1] > mean_tuple1[2]]  # find all means that comprise images after the current scene
        
        if len(later_images) == 0:
            continue
            
        mean_tuple2 = later_images[0]  # pick the first image, which should be closest in date
        mean_img2 = mean_tuple2[0]
        
        diff_out_name = os.path.join(diff_outdir, f"S1_{mean_tuple1[1].strftime('%Y%m%d')}-{mean_tuple1[2].strftime('%Y%m%d')}_Subtract_{mean_tuple2[1].strftime('%Y%m%d')}-{mean_tuple2[2].strftime('%Y%m%d')}.tif")
        
        pool.apply_async(upland_utils.calc_difference, args=(mean_img1, mean_img2, diff_out_name)) 

    pool.close()
    pool.join()
        
    
def main():
    args = __get_arg_parser().parse_args()
    
    indir = os.path.abspath(args.indir)
    
    tmpdir = os.path.abspath(args.tmpdir) if args.tmpdir is not None else None
    if tmpdir is not None:
        upland_utils.make_directory(tmpdir)
    
    outdir = os.path.abspath(args.outdir) if args.outdir is not None else indir
    upland_utils.make_directory(outdir)
    
    date_pos = args.date_pos - 1  # make date position zero-indexed

    if args.aoi is not None:
        clip_vector = os.path.abspath(args.aoi)
        if not os.path.isfile(clip_vector):
            raise ValueError('No such file: {}'.format(clip_vector))
    else:
        clip_vector = None
        
    threads = args.threads if args.threads < multiprocessing.cpu_count() else multiprocessing.cpu_count()

    run(indir, tmpdir, outdir, aoi=clip_vector, resolution=args.resolution, epsg=args.epsg, threads=threads, window_size=args.window_size, date_start_pos=date_pos)


if __name__ == "__main__":
    main()

