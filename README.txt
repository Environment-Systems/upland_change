1. Project overview 

These scripts were created as part of the DEFRA funded project SD1721- Upland burn monitoring through integration of Sentinel 1 and Sentinel 2. This project report built on a number of existing approaches for detecting and delineating heather burns from Sentinel satellites. The aim was to build on the existing work and evaluate the suitability of Sentinel and other datasets to assess if a reliable method of dating heather burns can be established. 

The scripts provided in this repository support the calculation of coherence from Sentinel-1 Single Look Complex (SLC) pairs, and detection of burn extent and timing from seasonal Sentinel-1 backscatter imagery.

2. Dependencies

- Python >= 3.6
- GDAL >= 2.2.3
- ESA SNAP > 8.0.7 


3. Installation

If git is installed, the reposisitory can be installed using git clone:

git clone https://<USERNAME>@bitbucket.org/Environment-Systems/upland_change.git

If the repository is downloaded as a zip archive from bitbucket, the relative folder structure must be kept as-is for the scripts to function correctly.

Additional python libraries are also required, these can be installed as needed using pip.


4. Running scripts

The scripts are run by calling them in the command line as follows:

python3/path/to/burn_detection_pt1_average.py <arguments>

where “/path/to/” is substituted to the directory containing the files.

Further information on how to use the scripts can be accessed by running:

python3/path/to/burn_detection_pt1_average.py --help


5. Description of scripts

a) Coherence processing: slc_processing.py

Calculates coherence for 6-day or 12-day Sentinel-1 image pairs. The script requires a single folder of SLC images for the desired study area. There is no need to sort by orbit or date as the script will identify all eligible pairs and calculate coherence. Imagery is re-projected into OSGB1936 (EPSG:27700).

***Notes on slc_processing.py***
	The script must be run with upland_utils.py in the same folder
	
	The snap_graph folder must be in in the same location as the slc processing script. 
	
	The snap_graph folder must contain  Merge_TerrainCorrect.xml, Slice_Assemble.xml and Subswath_Coherence.xml 
	
	settings.conf must be updated with the location of the SNAP gpt processor on your local machine. E.G. gpt_exe =  /home/name/snap/bin/gpt
	
	By default, exports will be of maximum possible extent and projected into the most appropriate UTM zone

	To run, first set "gpt_exe" in the accompanying settings.conf to your installation of SNAP's GPT processor
	e.g. gpt_exe = /home/name/snap/bin/gpt

	To run in command line:
		python3 </path/to/>slc_processing.py -i </path/to/s1/slc folder>

	For help with additional arguments, see
		python3 </path/to/>slc_processing.py --help
	
	Coherence generation is a processing, memory and storage intensive operation; a high-end work station may be required to run succesfully.

b) Burn change detection: burn_detection_pt1_average.py & burn_detection_pt2_thresholds.py

These two scripts run the automated burn detection process. The process is split into average and thresholding to allow for QA of average images (e.g. removing winter images agffected by snowfall) and to allow the threshold parameters to be modified without having to re-process all the input imagery. 

**Notes on burn_detection_pt1_average.py**
	The script must be run with upland_utils.py in the same folder

	Script should be simple to use, requiring only an input directory of LINEAR BACKSCATTER .tif files. Decibel imagery MUST be converted to linear scale before runing this script. 

	A temporary directory can optionally be provided to direct intermediate files to a faster drive. Outputs will be created within named folders in the output directory.
	 
	All images MUST be of the same dimensions in oder for calculations to take place. To aid with this, the user can provide an AOI to clip products to, as well as a target resolution and projection. Using all three of there will guarantee inputs are transformed to common extents. These arguments are optional, in case imagery has already been prepared in this manner.

	The user can adjust the date-pos to target the poistion of the date within the input filenames. The script has been designed for use with products folling the ESA convention (e.g. "S1A_IW_GRDH_1SDV_20201001T062920_20201001T062945_034599_040733_5AB9") where the date is accessible from the 18th character. If inputs are different to this format, please set the parameter accordingly

	The script allows mean and difference calculations to occur on multiple cores. Note, this will only multiply the number of images 
	that are processed simultaneously. The user should ensure that they have sufficient memory and hard drive capabilities to use 
	this to produce meaningful benefits.

	To run in command line:
		python3 </path/to/>burn_detection_pt1_average.py -i </path/to/backscatter_tiffs>

	For help with additional arguments:
		python3 </path/to/>burn_detection_pt1_average.py --help


**Notes on burn_detection_pt2_thresholds.py**
	Script requires only an input directory which MUST be the "02_Difference" output folder from the previous script.

	The script allows mean and difference calculations to occur on multiple cores. Note, this will only multiply the number of images 
	that are processed simultaneously. The user should ensure that they have sufficient memory and hard drive capabilities to use 
	this to produce meaningful benefits.

	To run in command line:
		python3 </path/to/>burn_detection_pt2_thresholds.py -i </path/to/>02_Difference

	For help with additional arguments, see
		python3 </path/to/>burn_detection_pt2_thresholds.py --help

c) upland_utils.py

Common utility functions for other scripts. Must be kept in same location as other scripts for them to run succesfully.