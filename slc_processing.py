"""
# # # # # # # # # # # # # #
:author Environment Systems Ltd.
:date 10/02/2022
The Software is provided "as is", with all faults, defects and errors, and without warranty of any kind.


Script to generate coherence for Sentinel-1 SLC products

Dependencies:
- Python >= 3.6
- GDAL >= 2.2.3
- ESA SNAP > 8.0.7  (other versions untested - there could be variable name differences with graphs in other SNAP versions)


Script should be simple to use, requiring only an input directory of S1 SLC .zips (obtained externally from ESA/Alaska Satellite Facility/etc.)
A temporary directory can optionally be provided to direct intermediate files to a faster drive
The script also allows te user to clip and reproject outputs. 
By default, exports will be of maximum possible extent and projected into the most appropriate UTM zone

To run, first set "gpt_exe" in the accompanying settings.conf to your installation of SNAP's GPT processor
e.g. gpt_exe = /home/name/snap/bin/gpt

Then run
python3 /path/to/slc_processing.py -i /path/to/s1/slcs

For help with additional arguments, see
python3 /path/to/slc_processing.py --help

=======
Version 0.1
+ Build main workflow of coherence processing

Version 1.0
+ Improve usability 
+ Convert outputs to TIFFs

Version 1.1
+ Re-work slice-assembly code to process on-the-fly, rather than all products at the beginning (greatly reducing runtime disk usage requirements)

# # # # # # # # # # # # # #
"""

import argparse
import configparser
import datetime
import os
import shutil
import subprocess
import tempfile
import upland_utils


SWATHS = ["IW1", "IW2", "IW3"]
POLARISATION = "VV"

SLICEASSEMBLE_XML = "Slice_Assemble.xml"
COHERENCE_XML = "Subswath_Coherence.xml"
MERGE_XML = "Merge_TerrainCorrect.xml"

SNAP_NODATA = 0
OUTPUT_NODATA = -9999

CONFIG_FILE =  os.path.join(os.path.dirname(os.path.realpath(__file__)), 'settings.conf')


def __get_arg_parser():
    parser = argparse.ArgumentParser(description="Runs S1 SLC Coherence processing ")

    parser.add_argument('-i', '--indir', required=True, help="(Required) Input directory to recursively search for SLC .zip files")
    
    parser.add_argument('-o', '--outdir', help="(Optional) Output directory to export Coherence Tiffs - if not provided, indir is used")
    parser.add_argument('-t', '--tmpdir', help="(Optional) Temporary directory to store intermediate files")
    
    parser.add_argument('-a', '--aoi', help="(Optional) Vector filepath to clip output images to")
    parser.add_argument('-e', '--epsg', help="(Optional) EPSG code (e.g. '27700') for output files. If not provided, an auto-UTM projection is used")
    
    parser.add_argument('--continue-on-error', action='store_true', help="If set, try to continue processing other products if error occurs")
    parser.add_argument('--overwrite', action='store_true', help="If set, overwrite output files if already present")

    return parser


def calc_assembled_output_basename(products_to_assemble):
    # if only 1 product in group, there's no assembly to do. Just record product location for next stage
    if len(products_to_assemble) == 1:
        return os.path.basename(products_to_assemble[0])
    
    first_product = os.path.basename(products_to_assemble[0])
    last_product = os.path.basename(products_to_assemble[-1])
    
    # replace product end datetime with that of last product in group
    return first_product.replace(first_product[33:48], last_product[33:48])


def get_s1_datetimes(s1_filename):
    """ 
    Extract S1 dates (as datetimes) from filename

    :param s1_filename: filename only of S1 product (e.g. S1A_IW_SLC__1SDV_20191002T062217_20191002T062244_029276_035397_C836.zip)
    :return: relative orbit integer
    """
    start_datetime = datetime.datetime.strptime(s1_filename[17:32], "%Y%m%dT%H%M%S")
    end_datetime = datetime.datetime.strptime(s1_filename[33:48], "%Y%m%dT%H%M%S")
    
    return start_datetime, end_datetime


def get_s1_rel_orbit(s1_filename):
    """ 
    Extract and convert S1 orbit to relative number

    :param s1_filename: filename only of S1 product (e.g. S1A_IW_SLC__1SDV_20191002T062217_20191002T062244_029276_035397_C836.zip)
    :return: relative orbit integer
    """
    sensor_letter = s1_filename[2]
    absolute_orbit = s1_filename[49:55]

    if 'A' == sensor_letter:
        return ((int(absolute_orbit)-73) % 175)+1
    elif 'B' == sensor_letter:
        return ((int(absolute_orbit)-27) % 175)+1
    else:
        raise ValueError(f"Cannot generate relative orbit: Sensor S1{sensor_letter} not known")


def get_slice_assembly_groups(s1_fileanmes):
    """ 
    Sort input files into groups of matching dates and orbits (ready for slice assembly)

    :param s1_fileanmes: a list of filepaths (optionally including directories) in format: S1A_IW_SLC__1SDV_20191002T062217_20191002T062244_029276_035397_C836
    :return: list product filenames, arranged into lists of matching dates
    """
    slice_groups = []
    remaining_candidates = s1_fileanmes.copy()
    
    for s1_filename in s1_fileanmes:
    
        # if current file has already been added to another group, move to next item
        if s1_filename not in remaining_candidates:
            continue
    
        # add file to new group and remove from candidates
        current_group = [s1_filename]
        remaining_candidates.remove(s1_filename)
        
        datetime1 = get_s1_datetimes(os.path.basename(s1_filename))[0]
        orbit1 = get_s1_rel_orbit(os.path.basename(s1_filename))
        
        for candidate in remaining_candidates:
            datetime2 = get_s1_datetimes(os.path.basename(candidate))[0]
            orbit2 = get_s1_rel_orbit(os.path.basename(candidate))
            
            # if any of the remaining products has the same date and orbit, add it to the group
            if (datetime1.date() == datetime2.date()) and (orbit1 == orbit2):
                current_group.append(candidate)
                remaining_candidates.remove(candidate)
                
        # add group of files to list of groups
        current_group.sort()
        slice_groups.append(current_group)
        
    return slice_groups
    
    
def get_coherence_pairs(s1_filenames):
    """ 
    Checks through all input files and finds the next (date after) closest available product with the same orbit
    Products are arranged into a dictionary with the key for each product and the value being the next closest scene
    The most recent product of each orbit will not have a next available product, so the dict value will be None
    
    :param s1_fileanmes: a list of filepaths (optionally including directories) in format: S1A_IW_SLC__1SDV_20191002T062217_20191002T062244_029276_035397_C836
    :return: dict with input filenames as keys and their closest available pair as values
    """
      
    pairs_dict = {}
    
    remaining_candidates = s1_filenames.copy()
    
    for s1_filename in s1_filenames:
        closest_pair = None 
    
        datetime1 = get_s1_datetimes(os.path.basename(s1_filename))[0]
        orbit1 = get_s1_rel_orbit(os.path.basename(s1_filename))
        
        closest_pair_datetime = datetime.datetime(9999, 1, 1)
        
        for candidate in remaining_candidates:
            datetime2 = get_s1_datetimes(os.path.basename(candidate))[0]
            orbit2 = get_s1_rel_orbit(os.path.basename(candidate))
            
            if candidate != s1_filename and orbit1 == orbit2 and datetime2 > datetime1 and datetime2 < closest_pair_datetime:
                closest_pair = candidate
                closest_pair_datetime = datetime2
         
        pairs_dict[s1_filename] = closest_pair
        if closest_pair is not None:
            remaining_candidates.remove(closest_pair)
        
    return pairs_dict
   
   
class RunCoherenceProcessing:
    def __init__(self, gpt_exe):
        self.gpt_exe = gpt_exe

    def run_slice_assembly_graph(self, infiles, outfile):
        processing_graph = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'snap_graphs', SLICEASSEMBLE_XML)
        gpt_command = [self.gpt_exe, processing_graph, f"-Pinputs={','.join(infiles)}", f"-Poutput={outfile}"]
        subprocess.check_call(gpt_command)
        
    def run_coherence_graph(self, infile1, infile2, outfile, swath, polarisation):
        processing_graph = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'snap_graphs', COHERENCE_XML)
        gpt_command = [self.gpt_exe, processing_graph, f"-Pinput1={infile1}", f"-Pinput2={infile2}", f"-Poutput={outfile}", f"-Pswath={swath}", f"-Ppolarisation={polarisation}"]
        subprocess.check_call(gpt_command)
        
    def run_merge_graph(self, infile1, infile2, infile3, outfile, polarisation):
        processing_graph = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'snap_graphs', MERGE_XML)
        gpt_command = [self.gpt_exe, processing_graph, f"-Pinput1={infile1}", f"-Pinput2={infile2}", f"-Pinput3={infile3}", f"-Poutput={outfile}", f"-Ppolarisation={polarisation}"]
        subprocess.check_call(gpt_command)


    def run(self, indir, outdir, tmpdir=None, epsg=None, clip_vector=None, continue_on_error=False, overwrite=False):
    
        temp_dir = tempfile.mkdtemp(dir=tmpdir)
        out_proj = None if epsg is None else f"EPSG:{epsg}"  # if provided, convert epsg code to GDAL-compatible format

        infiles = [os.path.join(root, f) for root, dirs, files in os.walk(indir) for f in files if f.endswith(".zip") and "SLC" in f]
        
        slice_groups = get_slice_assembly_groups(infiles)  
        
        assembled_dict = {}
        for slice_group in slice_groups:
            assembled_name = calc_assembled_output_basename(slice_group)
            assembled_dict[assembled_name] = slice_group
        
        coherence_pairs = get_coherence_pairs(list(assembled_dict.keys()))
        
        for coherence_pair in sorted(coherence_pairs):
            inputs1 = assembled_dict[coherence_pair]
            inputs2 = assembled_dict[coherence_pairs[coherence_pair]] if coherence_pairs[coherence_pair] is not None else None
            
            # if the input does not have a pair (i.e. latest availble date), skip to next product
            if inputs2 is None:
                continue
                
            temp_slice_dir = os.path.join(temp_dir, "00_SA")
            upland_utils.make_directory(temp_slice_dir)
            
            try:
                if len(inputs1) == 1:
                    product1 = inputs1[0]
                else:
                    product1 = os.path.join(temp_slice_dir, f"{os.path.splitext(coherence_pair)[0]}.dim")
                    self.run_slice_assembly_graph(inputs1, product1)
                    
                if len(inputs2) == 1:
                    product2 = inputs2[0]
                else:
                    product2 = os.path.join(temp_slice_dir, f"{os.path.splitext(coherence_pairs[coherence_pair])[0]}.dim")
                    self.run_slice_assembly_graph(inputs2, product2)
            except subprocess.CalledProcessError:
                # if either slice assembly fails, either move to the next set of inputs, or raise
                shutil.rmtree(temp_slice_dir)
                if continue_on_error:
                    continue  
                raise
                
            temp_swath_dir = os.path.join(temp_dir, "01_Swath")
            upland_utils.make_directory(temp_swath_dir)
                
            date1 = get_s1_datetimes(os.path.basename(product1))[0]
            date2 = get_s1_datetimes(os.path.basename(product2))[0]
            orbit = get_s1_rel_orbit(os.path.basename(product1))
            snap_basename = f"coh_{POLARISATION}_{date1.strftime('%d%b%Y')}_{date2.strftime('%d%b%Y')}"
            output_basename = f"S1_{POLARISATION}_{date1.strftime('%Y%m%d')}_{date2.strftime('%Y%m%d')}_Orb{orbit}_Coherence"
            
            print(f"Running coherence processing for {output_basename}...")
            
            swath_tracker = []
            for swath in SWATHS:
                temp_swath_output = f"{os.path.join(temp_swath_dir, output_basename)}_{swath}.dim"
                try:
                    print(f"Processing swath {swath}...")
                    self.run_coherence_graph(product1, product2, temp_swath_output, swath, POLARISATION)
                except subprocess.CalledProcessError:
                    shutil.rmtree(temp_slice_dir)
                    shutil.rmtree(temp_swath_dir)
                    if continue_on_error:
                        break  # if there's an error in any of the swaths, it must break this loop and move back to the next product
                    raise
                
                swath_tracker.append(temp_swath_output)  # add swath location to tracker
            
            shutil.rmtree(temp_slice_dir)
            temp_merge_dir = os.path.join(temp_dir, "02_Merge")
            upland_utils.make_directory(temp_merge_dir)
            
            merge_output = f"{os.path.join(temp_merge_dir, output_basename)}.dim"
            merge_output_image = os.path.join(merge_output.replace(".dim", ".data"), f"{snap_basename}.img")
            tmp_ouput_image = os.path.join(outdir, f"{output_basename}_tmp.tif")
            main_ouput_image = os.path.join(outdir, f"{output_basename}.tif")

            try:
                print(f"Merging coherence for {output_basename}...")
                self.run_merge_graph(swath_tracker[0], swath_tracker[1], swath_tracker[2], merge_output, POLARISATION)
                
                print(f"Converting output to {main_ouput_image}...")
                # TODO: this should be a single gdal call, but we noticed a significant offset in output when trying to clip and reproject in some ciscumstances 
                upland_utils.gdalwarp(merge_output_image, tmp_ouput_image, src_nodata=SNAP_NODATA, dst_nodata=OUTPUT_NODATA, clip_vector=clip_vector, overwrite=overwrite)  # clip to AOI
                upland_utils.gdalwarp(tmp_ouput_image, main_ouput_image, src_nodata=SNAP_NODATA, dst_nodata=OUTPUT_NODATA, dst_srs=out_proj, overwrite=overwrite)  # reproject
                
            except subprocess.CalledProcessError:
                if continue_on_error:
                    continue
                raise
            finally:
                if os.path.isfile(tmp_ouput_image):
                    os.remove(tmp_ouput_image)
                shutil.rmtree(temp_swath_dir)
                shutil.rmtree(temp_merge_dir)
        
        # now it's safe to remove any assembled products to reclaim some space    
        shutil.rmtree(temp_dir)


def main():
    args = __get_arg_parser().parse_args()
    
    config = configparser.ConfigParser()
    read_files = config.read([CONFIG_FILE])
    if not read_files:
        raise RuntimeError(f"Cannot find the configuration file: {CONFIG_FILE}")
    gpt_exe = config.get("sentinel", "gpt_exe")

    indir = os.path.abspath(args.indir)
    
    outdir = os.path.abspath(args.outdir) if args.outdir is not None else indir
    upland_utils.make_directory(outdir)

    if args.aoi is not None:
        clip_vector = os.path.abspath(args.aoi)
        if not os.path.isfile(clip_vector):
            raise ValueError('No such file: {}'.format(clip_vector))
    else:
        clip_vector = None

    gpt_processor = RunCoherenceProcessing(gpt_exe)
    gpt_processor.run(indir, outdir, tmpdir=args.tmpdir, epsg=args.epsg, clip_vector=clip_vector, continue_on_error=args.continue_on_error, overwrite=args.overwrite)


if __name__ == "__main__":
    main()

